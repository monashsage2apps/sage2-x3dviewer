/*
function addCSS(url, callback) {
	var fileref = document.createElement("link");

	if (callback) {
		fileref.onload = callback;
	}

	fileref.setAttribute("rel", "stylesheet");
	fileref.setAttribute("type", "text/css");
	fileref.setAttribute("href", url);
	document.head.appendChild(fileref);
}
*/
function addContent(url, divcontainer, callback) {

	var xhr= new XMLHttpRequest();
	xhr.open('GET', url, true);
	xhr.onreadystatechange= function() {
		if (this.readyState!==4) return;
		if (this.status!==200) return; // or whatever error handling you want
		divcontainer.innerHTML= this.responseText;
		if(callback)
			callback();
	};
	xhr.send();

}

var x3dviewer = SAGE2_App.extend({

    init : function(data) {
        this.SAGE2Init("div", data);
		this.resizeEvents = "onfinish"; 
		this.maxFPS = 1;


		this.z = 0;
		this.x = 0;
		this.y = 0;
		
		this.alpha = 0;
		this.beta = 0
		this.gamma = 0;

		this.translate = new x3dom.fields.SFVec3f(0, 0, 0);


		this.controls.addButton({label : "Reset View", identifier : "resetview", position:8});
		this.controls.addButton({label : "Next View", identifier : "nextview", position:1});
		this.controls.addButton({label : "Previous View", identifier : "prevview", position:3});
		this.controls.addButton({label : "Release Mouselock", identifier : "releaselock", position:7});
		this.controls.finishedAddingControls();
		this.enableControls = true;

		this.prepareContainer(data);
    },

	prepareContainer : function(data) {
		this.modeldiv = document.createElement('div');
		this.modeldiv.style.width = "100%";
		this.modeldiv.style.height = "100%";
		this.element.appendChild(this.modeldiv);
		
		addContent(this.resrcPath + "data/x3droottemplate", this.modeldiv, this.loadX3DModel.bind(this));
		this.element.style.background="#000000";

	},

	loadX3DModel : function() {
		this.x3droot = document.getElementById('x3droot');
		
		
		this.inlinemodel = document.getElementById("inlinemodel");


		this.x3droot.id = this.x3droot.id + this.id;
		
		
		this.inlinemodel.id = this.inlinemodel.id +  + this.id;
		
		this.x3droot.setAttribute('width', this.sage2_width);
		this.x3droot.setAttribute('height', this.sage2_height);

		if (this.state.file && this.state.file.length > 0) {
			// asset to load
			var server = this.resrcPath.match(/http.*?:\/\/.*?\//);
			if(server.length == 1)
				server = server[0];
			else
				server = '';
			var toLoad = server + this.state.file;

			this.inlinemodel.setAttribute('url', toLoad);

				
			x3dom.reload();
			
			var runtime = this.x3droot.runtime;

			
			/**
			 * Workaround: 
			 * setting the matrices once doesn't work, as is it ignored the first 
			 * frames. We check now for every drawn frame, if the x3dom matrices
			 * are the same as our state matrices.. If so, we cancel the forced update 
			 */
			this.count = 0;
			runtime.enterFrame = function(element) {
				console.log('frame ' + this.count++);
				var doc = this.x3droot.getElementsByTagName('canvas')[0].parent.doc;
				console.log('tmat: ' + doc._viewarea._transMat);
				var stack = doc._scene.getViewpoint()._stack;
				var active = stack.getActive();
				if(doc._viewarea._transMat.equals(this.state.transmat)
					&& doc._viewarea._rotMat.equals(this.state.rotmat)
					&& (this.state.activeview.length == 0 || (this.state.activeview.length > 0 && this.state.activeview == active._DEF))) {
					runtime.enterFrame = function(){};
					
				} else {
					if(this.state.activeview.length > 0)
						this.setViewport(this.state.activeview);
					this.updateModel();
				}
				//if(this.count++ > 3)
				
			}.bind(this);
			

		}
	},

    load : function(date) {

		this.updateModel();
		if(this.state.activeview.length > 0)
			this.setViewport(this.state.activeview);
		
    },

	updateModel : function() {
        console.log('updating model');
		if(this.x3droot === undefined)
			return;


		var listcanvas = this.x3droot.getElementsByTagName('canvas');
		if(listcanvas.length == 0)
			return;

		this.translate.x = this.state.translate.x;
		this.translate.y = this.state.translate.y;
		this.translate.z = this.state.translate.z;

		//console.log('load translate ' + this.translate);
		var doc = listcanvas[0].parent.doc;
		
		var rm = new x3dom.fields.SFMatrix4f();
		rm.setValues(this.state.rotmat);
		
		doc._viewarea._rotMat = rm;
		
		
		var tm = new x3dom.fields.SFMatrix4f();
		tm.setValues(this.state.transmat);
		doc._viewarea._transMat = tm;

		//console.log('load; ' + doc._viewarea._transMat);
		doc.needRender = true;

		console.log('model updated');
	},

	setViewport : function(name) {
		var doc = this.x3droot.getElementsByTagName('canvas')[0].parent.doc;
		// set active viewport
		if(name.length > 0) {
			var stack = doc._scene.getViewpoint()._stack;
			for(var idx in stack._bindBag) {
				if(stack._bindBag[idx]._DEF == name)
					stack.replaceTop(stack._bindBag[idx]);
			}

		}
	},

	resetView: function() {
		var doc = this.x3droot.getElementsByTagName('canvas')[0].parent.doc;		
		this.state.translate = new x3dom.fields.SFVec3f(0, 0, 0);
		this.state.transmat = new x3dom.fields.SFMatrix4f();
		this.state.rotmat = new x3dom.fields.SFMatrix4f();

		this.translate = this.state.translate;
		doc._viewarea._rotMat = this.state.rotmat;
		doc._viewarea._transMat = this.state.transmat;
		doc.needRender = true;

		this.refresh();
	},

    draw : function(date) {
		
		//this.updateModel();
		
    },

    resize : function(date) {
		this.x3droot.setAttribute('width', this.sage2_width);
		this.x3droot.setAttribute('height', this.sage2_height);


    },

    event : function(eventType, position, user, data, date) {

		var doc = this.x3droot.getElementsByTagName('canvas')[0].parent.doc;

		if(eventType == 'widgetEvent') {
			switch(data.identifier) {
				case 'resetview' : 
					this.resetView();
					break;
				case "nextview":
					this.x3droot.runtime.nextView();
					this.resetView();
					break;
				case "prevview":
					this.x3droot.runtime.prevView();
					this.resetView();
					break;
				case "releaselock":
					this.state.mouselock = "";
					break;

			}
						
			var stack = doc._scene.getViewpoint()._stack;
			var active = stack.getActive();
			this.state.activeview = active._DEF;
			this.refresh();
			console.log(active);
			return;
		}

		/*
			don't allow mouse events, if the lock has been set
		*/
		if(this.state.mouselock.length > 0 
			&& this.state.mouselock !== user.id) {
			// check, if the user's pointer holding the lock
			// was disabled (conneciton lost etc.) inbetween
			var pointer = document.getElementById(this.state.mouselock);
			if(pointer !== undefined
				&& pointer.style.display != 'none')
				return;
			else {
				this.state.mouselock = user.id;
				this.dragging = false;
			}
		}
		if(eventType == 'pointerPress' && data.button == 'left') {
			this.dragging = true;
			this.state.mouselock = user.id;

		}
		if(eventType == 'pointerRelease' && data.button == 'left') {
			this.dragging = false;
			this.state.mouselock = "";
		}

		if(eventType == 'pointerMove' && this.dragging) {
			if(this.rotate == true) {
				this.beta = (data.dx) / 100;
				this.gamma = (data.dy) / 100;

				var omegamagnitute = 1;
				if(omegamagnitute == 0)
					omegamagnitute = 1;
				var axis1 = this.beta / omegamagnitute;
				var axis2 = this.gamma / omegamagnitute;
				
				var halfangle = omegamagnitute / 2;
				var sinThetaOverTwo = Math.sin(halfangle);
				var cosThetaOverTwo = Math.cos(halfangle);

				
				var viewpoint = doc._scene.getViewpoint();

				var center = viewpoint.getCenterOfRotation();

				var mat = doc._viewarea.getViewMatrix();
				mat.setTranslate(new x3dom.fields.SFVec3f(0,0,0));

				
				
				var rotmat = doc._viewarea._rotMat;
				
				var rotationX = x3dom.fields.SFMatrix4f.rotationX(axis2);
				var rotationY = x3dom.fields.SFMatrix4f.rotationY(axis1);
				//console.log(rotation);

				rotmat = rotmat
					.mult(x3dom.fields.SFMatrix4f.translation(center))
					.mult(mat.inverse())
					.mult(rotationX)
					.mult(rotationY)
					.mult(mat)
					.mult(x3dom.fields.SFMatrix4f.translation(center.negate()));


				doc._viewarea._rotMat = rotmat;
				//console.log(rotmat);
				doc.needRender = true;

				this.state.rotmat = doc._viewarea._rotMat;
				

			} else {
				this.x = (data.dx) * 100;
				this.y = (data.dy) * 100;
				this.z = 0;
				
				var speed = (doc._scene._lastMax.subtract(doc._scene._lastMin)).length(); 
				speed = (speed < x3dom.fields.Eps ? 1 : speed);
				var move = new x3dom.fields.SFVec3f((data.dx * speed) / doc._viewarea._width, -(data.dy * speed) / doc._viewarea._height, 0);

				var m = new x3dom.fields.SFMatrix4f();
				var viewpointmatrix = doc._viewarea.getViewpointMatrix();
				//console.log('translate ' + this.translate);
				this.translate = this.translate.add(move);

				var mat = viewpointmatrix.mult(doc._viewarea._transMat);
				//console.log('before tmat: ' + doc._viewarea._transMat);
				doc._viewarea._transMat = mat.inverse().mult(x3dom.fields.SFMatrix4f.translation(this.translate)).mult(mat);
				//console.log('after tmat: ' + doc._viewarea._transMat);
				doc.needRender = true;

				this.state.transmat = doc._viewarea._transMat;
				this.state.translate = this.translate;
				

			}
		}
		if(eventType == 'pointerScroll') {
			var doc = this.x3droot.getElementsByTagName('canvas')[0].parent.doc;

			var speed = (doc._scene._lastMax.subtract(doc._scene._lastMin)).length(); 
			speed = (speed < x3dom.fields.Eps ? 1 : speed);
			var zmove = new x3dom.fields.SFVec3f(0, 0, ((data.wheelDelta / 10)  * speed) / doc._viewarea._height);


			var viewpointmatrix = doc._viewarea.getViewpointMatrix();

			this.translate = this.translate.add(zmove);

			var mat = viewpointmatrix.mult(doc._viewarea._transMat);

			doc._viewarea._transMat = mat.inverse().mult(x3dom.fields.SFMatrix4f.translation(this.translate)).mult(mat);
			//console.log('tmat: ' + doc._viewarea._transMat);
			doc.needRender = true;

			this.state.transmat = doc._viewarea._transMat;
			this.state.translate = this.translate;
			

		}
		if(eventType == 'specialKey') {
			if(data.code === 17 && data.state == 'down')
				this.rotate = true;
			if(data.code === 17 && data.state == 'up')
				this.rotate = false;
		}

		this.refresh();
    }
});
